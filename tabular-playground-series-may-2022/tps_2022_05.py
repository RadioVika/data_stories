import pandas as pd
import os

from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score
import lightgbm as lgbm

pd.options.display.max_columns = 250

TRAIN_DATA_FILENAME = 'train.csv'
TEST_DATA_FILENAME = 'test.csv'
SAMPLE_SUBMISSION_FILENAME = 'sample_submission.csv'
DATA_FOLDERNAME = 'data'
SUBMISSION_FOLDERNAME = 'submissions'
train_columns = ['f_00', 'f_01', 'f_02', 'f_03', 'f_04', 'f_05', 'f_06', 'f_07',
                 'f_08', 'f_09', 'f_10', 'f_11', 'f_12', 'f_13', 'f_14', 'f_15', 'f_16',
                 'f_17', 'f_18', 'f_19', 'f_20', 'f_21', 'f_22', 'f_23', 'f_24', 'f_25',
                 'f_26', 'f_28', 'f_29', 'f_30', 'char_unique_num', 'ch0', 'ch1',
                 'ch2', 'ch3', 'ch4', 'ch5', 'ch6', 'ch7', 'ch8', 'ch9']
model_params = {'n_estimators': 5000, 'random_state': 1}


def add_features(df):
    ''' Add new features for string variable'''
    df['char_unique_num'] = df['f_27'].apply(set).apply(len)

    for i in range(10):
        df['ch' + str(i)] = df['f_27'].str[i].apply(ord) - ord('A')

    df['f_02_21'] = df.f_21 + df.f_02
    df['f_00_01_26'] = df.f_00 + df.f_01 + df.f_26

def train_model(train, params):
    # train test split
    X = train[train_columns]
    y = train['target'].values
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, stratify=y, random_state=1)

    # train model
    clf = lgbm.LGBMClassifier(**model_params)
    clf.fit(X_train, y_train)

    # evaluate model
    y_pred = clf.predict_proba(X_test)[:, 1]
    print('ROC AUC:', roc_auc_score(y_test, y_pred))

    return clf

if __name__ == "__main__":
    print('Load data ...')
    train = pd.read_csv(os.path.join(DATA_FOLDERNAME, TRAIN_DATA_FILENAME))
    test = pd.read_csv(os.path.join(DATA_FOLDERNAME, TEST_DATA_FILENAME))
    submit = pd.read_csv(os.path.join(DATA_FOLDERNAME, SAMPLE_SUBMISSION_FILENAME))

    print('Add new features ...')
    for df in [train, test]:
        add_features(df)

    print('Train model ...')
    # train model
    clf = train_model(train, model_params)

    # submit results
    print('Prepare submission file ...')
    submit['target'] = clf.predict_proba(test[train_columns])[:, 1]
    submit.to_csv(os.path.join(SUBMISSION_FOLDERNAME, 'submission_05.csv'), index=False)
    print('submission_05.csv is ready!')



