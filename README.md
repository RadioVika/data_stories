# Competitions
[Tabular Playground Series - May 2022](https://www.kaggle.com/competitions/tabular-playground-series-may-2022)

[My solution](https://gitlab.com/RadioVika/data_stories/-/tree/main/tabular-playground-series-may-2022)
EDA, LightGBM, SHAP Values interactions.
ROC-AUC 0.99
